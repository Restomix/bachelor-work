﻿$(function () {
    $("#real-time").on("change", function () {
        if ($(this).is(":checked")) {
            interval = setInterval(() => {
                BuildPlot();
            }, 1000);
        }
        else {
            if (interval != null) {
                clearInterval(interval);
            }
        }
    });

});
$(function () {
    $("#guest-sensors").on("change", function () {
        var url = document.location.origin + "/Sensors/ChangeStateForGuestSensors";
        if ($(this).is(":checked")) {

            $.post(url, { state: true }).always(() => {
                let result = new Promise((resolve, reject) => {
                    setTimeout(() => {
                        UpdateSensorsTable();
                        resolve("ok");
                    }, 1000);
                });
               
            });
        }
        else {

            $.post(url, { state: false }).always(() => {
                let result = new Promise((resolve, reject) => {
                    setTimeout(() => {
                        UpdateSensorsTable();
                        resolve("ok");
                    }, 1000);
                });

            });
        }
        
    });
});