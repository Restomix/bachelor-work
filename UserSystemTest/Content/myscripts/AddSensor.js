﻿$('#submitButton').click(function () {
    var errors = 0;
    $("#menu2 :input").not("button").map(function () {
        if (!$(this).val()) {
            $(this).addClass('warning');
            errors++;
        } else if ($(this).val()) {

            $(this).removeClass('warning');
        }
    });
    if (errors > 0) {
        $('#errorwarn').text("All fields are required");
    }
    else
       
    $('#errorwarn').removeClass('warning').addClass('success').text("Success");   
    PostSensor();
    $(':input', '#menu2').val('');
});
function PostSensor() {

    var description = $('input[name=Description]').val();
    var phoneNumber = $("input[name='PhoneNumber']").val();
    var location = $("input[name='Location']").val();
    var id = $("input[name='ID']").val();

    var Data = { Description: description, PhoneNumber: phoneNumber, Location: location, ID: id };
    var url = document.location.origin + "/Sensors/PostSensor";
    $.post(url, Data);

}