﻿$("#SendUser").click(function () {
    var url = document.location.origin + "/Settings/PostUser/";
    var data = $("#UserNameInput option:selected").text();
    $.post(url, { name: data }, () => {

        UpdateUserTable();
        location.reload();
    });
   
});
$("#SaveEmailButton").click(function () {

    var EmailText = $("#inputEmail").val();
    var Notification = $("#UserNotificationInput option:selected").val();
    var url = document.origin + "/Settings/PostPhoneForNotifications/";
    $.post(url, { Email: EmailText, Notification: Notification }, function () {
        alert("Email and notifications is saved. Thank you.");
    });

});
UpdateUserTable();
function UpdateUserTable() {

    var url = document.location.origin + "/Settings/GetUsers/";
    
    var post = $.getJSON(url, function (data) {
       
            $("#SettingsTable tbody > tr").remove();
            for (var i = 0; i < data.length; i++) {
                $("<tr><td>" + data[i].name + "<p class='btn' id='deleteUser'><span class='glyphicon glyphicon-remove'></span></p>" + "<td/><tr/>").appendTo("#SettingsTable tbody");
            }
            $("#deleteUser").click(function () {
                var url = document.location.origin + "/Settings/DeleteUser/";
                var data = { name: $(this).parent().text() };
                $.post(url, data, function (a, b, c) { UpdateUserTable(); });
                location.reload();
            });
        

    });
}


