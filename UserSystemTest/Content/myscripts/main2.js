﻿var mymap = L.map('mapid').setView([51.505, -0.09], 13);
var marker;
var markers = new Array();
var selectedID = null;
var interval = null;
var sensorTable = null;
var dataTable = null;
var roleCustumer = null;
function CheckCustumer(){
    $.getJSON(document.location.origin + "/Sensors/GetRole", function (data) {
        if (data.Role == "true") {
            roleCustumer = true;
        }
        else {
            roleCustumer = null;
        }
    });
}
$.ajaxSetup({
    cache: false
})

UpdateTables(); 
CheckCustumer();
UpdateSensorsTable();
function UpdateSensorsTable()
{
    var url = document.location.origin + "/Sensors/GetSensors";
    $.getJSON(url, function (data) {
        if (sensorTable != null) { sensorTable.destroy(); }
        $("#SensorsTable tbody > tr").remove();
        if (markers !== null) {
            for (var i = 0; i < markers.length; i++) {
                mymap.removeLayer(markers[i]);
            }
        }
    
        for (var i = 0; i < data.length; i++) {

            var lat = data[i].Location.split("x")[0];
            var long = data[i].Location.split("x")[1];
            var id = data[i].NameID;
            var realid = data[i].SensorID;
            var status = data[i].Status;
            var url = document.location.origin + "/SecondSensors/Index";
            var editurl = "<a href='" + url + "'> <img class='edit' ' style='float:right;' src='https://cdn4.iconfinder.com/data/icons/ikooni-outline-seo-web/128/seo-42-32.png'/> </a>";    
            if (roleCustumer==null) {
                editurl = "";
            }
            var tr = $("<tr/>").attr("class", "sensor").html("");
            var td1 = $("<td/>").attr("style", "display:none;").html(data[i].SensorID);
            var td6 = $("<td/>").html(data[i].NameID);
            var td2 = $("<td/>").html(data[i].PhoneNumber);
            var td3 = $("<td/>").attr("class", "location").html(data[i].Location);
            var td4 = $("<td/>").html(data[i].Description);
            var td5 = $("<td/>").html("<img src='" + getSensorIconByStatusForTable(status) + "'>" + editurl + "");
           
            

            tr.append(td1).append(td6).append(td2).append(td3).append(td4).append(td5).appendTo($("#SensorsTable tbody"));

           // $("<tr class='sensor' ><td>" + data[i].SensorID + "</td><td>" + data[i].PhoneNumber + "</td><td class='location'>" + data[i].Location + "</td><td>" + data[i].Description + "</td><td><img src ='" + getSensorIconByStatusForTable(status) + "'> </td>" + "</tr>").appendTo($("#SensorsTable tbody"));


            var icon = L.icon({
                iconUrl: getSensorIconByStatus(status),
                iconSize: [40, 40],
                iconAnchor: [20, 40],
                popupAnchor: [0, -40]
            });

            var mark = new L.marker([lat, long], {icon: icon });
            mark.lat = lat.trim();
            mark.long = long.trim();
            mark.options.id = id;
            mymap.setView([lat, long]);
            // mymap.addLayer(mark); 
            mark.addTo(mymap);
            markers.push(mark);
            mark.bindPopup(data[i].Description);
       
           
        }
        $('.sensor').click(function () {
            
            var loc = $(this).find(".location").text();
            var lat = loc.split('x')[0];
            var long = loc.split('x')[1];
            mymap.closePopup();
            for (let i = 0; i < markers.length; i++) {

                if (markers[i]._latlng.lat == lat && markers[i]._latlng.lng == long) {
                    mymap.setView([lat, long]);
                    markers[i].openPopup();
                    break;
                }

            }
            selectedID = $(this).children("td").first().text();
            BuildPlot();
           
        });
        sensorTable = $("#SensorsTable").DataTable(
            {
                "createdRow": function (row, data, index) {
                   
                    $('td', row).addClass("sensor");
                    
                }
            
            }
        );
       
    }).fail(function () { $("#SensorsTable tbody > tr").remove();});
      
   
}
function UpdateTables() {
    $("#DivForPlot").hide();
    UpdateDataTable();
}

$("a[data-toggle='tab']").on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
        //selector for SENSORS
    if (target === "#menu1") {
        UpdateSensorsTable();
            
    }
        if (target === "#home")
        {   
            UpdateDataTable();
        }
 });
function BuildPlot()
{
    var url = document.location.origin + "/Data/GetDatasForPlot/" + selectedID;
    var answ = $.getJSON(url, function (data) {
        CalculateAverageData(data);
        var ArrayDates = new Array();
        var ArrayWater = new Array();
        var ArrayBatery = new Array();
        Plotly.purge("DivPlotId");
        for (var i = 0; i < data.length; i++) {
            ArrayDates.push(data[i].WhenIsAddedTime);
            ArrayBatery.push(data[i].Batery);
            ArrayWater.push(data[i].Water);
        }
        var Batery = {
            x: ArrayDates,
            y: ArrayBatery,
            type: 'scatter',
            name: "Batery"
        };

        var Water = {
            x: ArrayDates,
            y: ArrayWater,
            type: 'scatter',
            name: 'Water'
        };

        var data = [Water, Batery];
        $("#DivForPlot").show();
        Plotly.plot('DivPlotId', data);


    });
    answ.fail(() => { $("#DivForPlot").hide(); });   

}
function UpdateDataTable()
{
    var url = document.location.origin + "/Data/GetLastDatas";
    $.getJSON(url, function (data) {
       
        $("#LastDatasTable tbody > tr").remove();
        for (var i = 0; i < data.length; i++) {
            $("<tr class='LastData' ><td>" + data[i].NameID + "</td><td>" + data[i].WhenIsAddedTime + "</td><td class='Location'>" + data[i].Batery + "</td><td>" + data[i].Water + "</td></tr>").appendTo($("#LastDatasTable tbody"));

        }
     
        $("#LastDatasTable").DataTable();

    });
   
}
function getSensorIconByStatus(Status = 0) {

    if (Status == 0)
        return '/Content/Images/level1.png';
    if (Status == 1)
        return '/Content/Images/level2.png';
    if (Status == 2)
        return '/Content/Images/level4.png';
    if (Status == 3)
        return '/Content/Images/marker.png';
    return '/Content/Images/marker.png';
}
function getSensorIconByStatusForTable(Status = 0) {

    if (Status == 0)
        return '/Content/Images/GreenStatus.png';
    if (Status == 1)
        return '/Content/Images/YellowStatus.png';
    if (Status == 2)
        return '/Content/Images/RedStatus.png';
    if (Status == 3)
        return '/Content/Images/GrayStatus.png';
    return '/Content/Images/GrayStatus.png';
}


