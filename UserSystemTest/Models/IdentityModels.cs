﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.DynamicData;
using System.Diagnostics;
using System;
using System.Security.Principal;
using System.Net.Mail;
using System.Timers;
using System.Linq;
namespace UserSystemTest.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            //  Claim c2 = new Claim("http://example.org/claims/complexcustomclaim", new MyResourceType("Martin", 38), Rights.PossessProperty);
            return userIdentity;
        }
        public string Role { get; set; }
    }

    public enum SensorStatus { Green, Yellow, Red, Unresponsive, Online }
    public enum Notification { OnceAday, EveryThreeDays, OnceAWeek, EveryMonth, OnceOccuresError, DangerLevelWaterOrBatery }
    public enum Roles { Administrator, Custumer, Worker, Guest }
    public interface IUser
    {
        string Name { get; set; }
        string Email { get; set; }
        bool? showGuestSensors { get; set; }
    }
    public class Administrator : IUser
    {
        public int AdministratorID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool? showGuestSensors { get; set; }
        public virtual ICollection<Sensor> Sensors { get; set; }
    }
    public class Custumer : IUser
    {
        public int CustumerID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string EmailForNotifications { get; set; }
        public bool? showGuestSensors { get; set; }
        public Notification notification { get; set; }
        public virtual ICollection<Sensor> Sensors { get; set; }
        public virtual ICollection<Worker> Workers { get; set; }
    }
    public class Worker : IUser
    {
        public int WorkerID { get; set; }
        public int? CustumerID { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public bool? showGuestSensors { get; set; }
        public virtual Custumer Custumer { get; set; }

    }
    public class Sensor
    {
        public int SensorID { get; set; }
        public int? CustumerID { get; set; }
        public int? AdministratorID { get; set; }
        public string NameID { get; set; }
        public bool? IsForGuests { get; set; }
        public SensorStatus Status { get; set; }
        [DisplayName("Installation Time")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime InstallationTime { set; get; }
        [DisplayName("Phone number")]
        public string PhoneNumber { get; set; }
        [DisplayName("Location")]
        public string Location { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        public string CustumerName { get; set; }
        public virtual Custumer Custumer { get; set; }
        public virtual ICollection<Data> Datas { get; set; }
    }
    public class Data
    {
        public int DataID { get; set; }
        public int SensorID { get; set; }
        [DisplayName("Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime WhenIsAddedTime { set; get; }

        [DisplayName("Batery")]
        public int Batery { get; set; }

        [DisplayName("Water")]
        public int Water { get; set; }

        public virtual Sensor Sensor { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Custumer> Custumers { set; get; }
        public DbSet<Worker> Workers { get; set; }
        public DbSet<Sensor> Sensors { set; get; }
        public DbSet<Data> Data { get; set; }
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

        }
        public ICollection<Sensor> GetSensersByUserName(string name)
        {
            Worker worker = this.GetWorker(name);
            Custumer custumer = null; Administrator administrator = null;
            if (worker == null)
            {
                custumer = this.GetCustumer(name);

                if (custumer == null)
                {
                    administrator = this.GetAdministrator(name);
                }
            }
            
            if (worker != null)
            {
                return worker.showGuestSensors == true ? ContactUserAndGuestSensors(worker.Custumer?.Sensors) : worker.Custumer?.Sensors;
            }
            if (custumer != null)
            {
                return custumer.showGuestSensors == true ? ContactUserAndGuestSensors(custumer?.Sensors) : custumer?.Sensors;
            }
            if (administrator != null)
            {
                return administrator.showGuestSensors == true ? ContactUserAndGuestSensors(administrator?.Sensors) : administrator?.Sensors;
            }

            return this.GetGuestSensors();
        }
        public List<Sensor> ContactUserAndGuestSensors(ICollection<Sensor> sensor)
        {

            if (sensor != null)
            {
                List<Sensor> list = new List<Sensor>();
                list.AddRange(sensor);
                foreach (var item in this.GetGuestSensors())
                {
                    if (!sensor.Contains(item))
                    {
                        list.Add(item);
                    }
                }
                return list;
            }
            return this.GetGuestSensors();
        }
    }
    public static class Util
    {
        public static ApplicationDbContext db { get; set; }
        public static IUser GetUserByName(this ApplicationDbContext ex, string name)
        {
            return (IUser)ex.GetWorker(name) ?? (IUser)ex.GetCustumer(name) ?? (IUser)ex.GetAdministrator(name);
        }
        public static Administrator GetAdministrator(this ApplicationDbContext ex, string name)
        {
            return ex.Administrators.ToListAsync().Result?.Find(x => x?.Email == name);
        }
        public static Custumer GetCustumer(this ApplicationDbContext ex, string name)
        {
            return ex.Custumers.ToListAsync().Result?.Find(x => x?.Email == name);
        }
        public static Worker GetWorker(this ApplicationDbContext ex, string name)
        {
            return ex.Workers.ToListAsync().Result?.Find(x => x?.Email == name);
        }
        public static List<Sensor> GetGuestSensors(this ApplicationDbContext ex)
        {
            return ex.Sensors.ToListAsync().Result.FindAll(x => x.IsForGuests == true);
        }
        public static List<Sensor> GetSensors(this IPrincipal ex)
        {
            if (ex.Identity.IsAuthenticated)
            {
                return (List<Sensor>)db.GetSensersByUserName(ex.Identity.Name);
            }
            return db.GetGuestSensors();
        }
        public static Custumer GetCustumer(this IPrincipal ex)
        {
            return db.GetCustumer(ex.Identity.Name);
        }
        public static Worker GetWorker(this IPrincipal ex)
        {
            return db.GetWorker(ex.Identity.Name);
        }
        public static Administrator GetAdministrator(this IPrincipal ex)
        {
            return db.GetAdministrator(ex.Identity.Name);
        }
        public static Sensor GetSensorByData(this IPrincipal ex, Data data)
        {
            return db.Sensors.ToListAsync().Result?.Find(x => x.SensorID == data.SensorID);
        }
        public static class MailSender
        {
            static Timer clock;
            static DateTime startTime;
            public static void SenderInit()
            {

                startTime = DateTime.Now;
                clock = new Timer(TimeSpan.FromHours(24).TotalMilliseconds);
                clock.Elapsed += Clock_Elapsed;
                clock.AutoReset = true;

            }
            public static int GetDaysFromStart()
            {
                return (int)(DateTime.Now - startTime).TotalDays;
            }
            public static void StartSender()
            {
                clock.Start();
            }
            public static void StopSender()
            {
                clock.Close();
            }
            private static void Clock_Elapsed(object sender, ElapsedEventArgs e)
            {
                foreach (var item in db.Custumers.ToListAsync().Result)
                {
                    var sensors = item.Sensors.ToList();
                    if (item.notification == Notification.OnceOccuresError)
                    {
                        if (HtmlBodyForData(sensors, true) != string.Empty )
                        SendMail(item.EmailForNotifications, HtmlBodyForData(sensors,true).Replace("%",""));
                    }
                    else if (item.notification == Notification.OnceAday)
                    {
                        SendMail(item.EmailForNotifications, HtmlBodyForData(sensors));
                    }
                    else if (GetDaysFromStart() % 3 == 0 && item.notification == Notification.EveryThreeDays)
                    {
                        SendMail(item.EmailForNotifications, HtmlBodyForData(sensors));
                    }
                    else if (GetDaysFromStart() % 7 == 0 && item.notification == Notification.OnceAWeek)
                    {
                        SendMail(item.EmailForNotifications, HtmlBodyForData(sensors));
                    }
                    else if (GetDaysFromStart() % 31 == 0 && item.notification == Notification.EveryMonth)
                    {
                        SendMail(item.EmailForNotifications, HtmlBodyForData(sensors));
                    }
                }
            }
            public static void SendMail(string Email, string BodyHtml)
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(Email);
                mail.From = new MailAddress("watertracking@gmail.com");
                mail.Subject = "Sensor tracking";
                string Body = @"<html>
<body>
<h1 align='center'> Sensor notification </h1>
" + BodyHtml + @"
<footer>
<p>Posted by: Savchuk Yurii</p>
<p>Contact information: <a href='mailto: watertracking@gmail.com'>
watertracking@gmail.com </a>.</p></footer>
</body>
</html>
";
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                ("watertracking@gmail.com", "22081997y");// Enter seders User name and password
                smtp.EnableSsl = true;
                smtp.Send(mail);

            }
            public static string HtmlForWhenWaterOrBateryIsLow(Data data)
            {
                string body = string.Format(@"<h1> The sensor with ID={0}, has danger data </h1></br> Water:{1} <br/> Batery:{2}", data.Sensor.NameID, data.Water, data.Batery);
                return body;
            }
            public static string HtmlBodyForData(List<Sensor> sensors, bool error = false)
            {
                string row = string.Empty;
                string errorString = string.Empty;
                for (int i = 0; i < sensors.Count; i++)
                {
                    Data last = sensors[i]?.Datas?.ToList()?.FindLast(x => x != null);
                    if (last != null)
                    {
                        if ((DateTime.Now - last.WhenIsAddedTime).TotalDays >= 2)
                        {
                            errorString += string.Format(@"<p> The sensor with ID = {0}, did not answer for about 2 days, last data was taken at {1}</p><br/>", sensors[i].NameID,last.WhenIsAddedTime.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                        row += string.Format(@"<tr><td>{0}</td><td>{1}%</td><td>{2}%</td><td>{3}</td></tr>", sensors[i].NameID, last.Batery, last.Water, last.WhenIsAddedTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else
                    {
                        row += string.Format(@"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", sensors[i].NameID, "no notes", "no notes ", "no notes");
                    }
                }
                string Body = string.Format(@"<thead><tr><th>Sensor ID</th> <th>Batery</th><th>Water</th><th>Time</th></tr></thead><tbody>{0}</tbody>", row);

                if (error) { return errorString; }
                return Body;

            }
        }
    }

}