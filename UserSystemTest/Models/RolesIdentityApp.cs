﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UserSystemTest.Models
{
    public class AppDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var role1 = new IdentityRole { Name = "Administrator" };
            var role2 = new IdentityRole { Name = "Custumer" };
            var role3 = new IdentityRole { Name = "Worker" };

            roleManager.Create(role1);
            roleManager.Create(role2);
            roleManager.Create(role3);

            var admin = new ApplicationUser { Role = "Administrator", Email = "admin@gmail.com", UserName = "admin@gmail.com" };
            var custumer = new ApplicationUser { Role = "Custumer", Email = "novostragonlakos@gmail.com", UserName = "novostragonlakos@gmail.com" };
           
            var result = userManager.Create(admin, "22081997");
            var result2 = userManager.Create(custumer, "22081997");
            context.Administrators.Add(new Administrator()
            {
             showGuestSensors = true,
             Name ="admin@gmail.com",
             Email = "admin@gmail.com"
            }
            );
            context.Custumers.Add(new Custumer()
            {
                showGuestSensors = true,
                Name = "novostragonlakos@gmail.com",
                Email = "novostragonlakos@gmail.com",
                EmailForNotifications= "novostragonlakos@gmail.com"
            }
            );
            // если создание пользователя прошло успешно
            if (result.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);

               userManager.AddToRole(custumer.Id,role2.Name);
              
            }

            base.Seed(context);
        }
    }
}