﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UserSystemTest.Models;

namespace UserSystemTest.Controllers
{
    public class SensorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public Administrator currentAdministrator { get { return db.Administrators.FirstOrDefault(x => x.Name == User.Identity.Name); } }
        public Custumer currentCustumer { get { return db.Custumers.FirstOrDefault(x => x.Name == User.Identity.Name); } }
        public Worker currentWorker { get { return db.Workers.FirstOrDefault(x => x.Name == User.Identity.Name); } }
        public bool isWorker
        {
           get { return User.IsInRole("Worker"); }
        }
        public bool isCustumer
        {
           get { return User.IsInRole("Custumer"); }
        }
        public bool isAdministrator
        {
            get { return User.IsInRole("Administrator"); }
        }
        public void UpdateStatusForAllSensors(List<Sensor> sensors)
        {
            foreach (var item in sensors)
            {
                int count = 0;
                Data last = null;
                
                foreach (var item2 in item.Datas)
                {
                    count++;
                    last = item2;
                }
                if (count == 0)
                {
                    item.Status = SensorStatus.Unresponsive;
                }
                //unresponsive when more than 2 days there is none data
                else if((DateTime.Now - last.WhenIsAddedTime).TotalDays > 2)
                {
                    item.Status = SensorStatus.Unresponsive;
                }
                else if (last.Water < 10 || last.Batery < 10)
                {
                    
                    item.Status = SensorStatus.Red;
                }
                else if(last.Water < 50 && last.Batery < 50)
                {
                    item.Status = SensorStatus.Yellow;
                }
                else
                {
                    item.Status = SensorStatus.Green;
                }
               
                
            }
        }
        public HttpStatusCodeResult ChangeStateForGuestSensors( bool state)
        {
            if (ModelState.IsValid)
            {
                db.GetUserByName(User.Identity.Name).showGuestSensors = state;
                db.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        public ActionResult GetRole()
        {
            return Json(new { Role = User.IsInRole("Custumer") ? "true" : "false" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSensors()
        {
            UpdateStatusForAllSensors(db.Sensors.ToList());

            if (isCustumer)
            {
                var sensors = db.GetSensersByUserName(User.Identity.Name)?.Select(x => new { Status=x.Status, SensorID = x.SensorID, NameID = x.NameID, Location = x.Location, PhoneNumber = x.PhoneNumber, Description = x.Description });
                return Json(sensors, JsonRequestBehavior.AllowGet);
            }
            if (isWorker)
            {
                var sensors = db.GetSensersByUserName(User.Identity.Name)?.Select((x => new { Status = x.Status, SensorID = x.SensorID, NameID = x.NameID, Location = x.Location, PhoneNumber = x.PhoneNumber, Description = x.Description }));
                return Json(sensors, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var sensors = db.GetSensersByUserName(User.Identity.Name)?.Select((x => new { Status = x.Status, SensorID = x.SensorID, NameID = x.NameID, Location = x.Location, PhoneNumber = x.PhoneNumber, Description = x.Description }));
                return Json(sensors, JsonRequestBehavior.AllowGet);
            }

        }
        public HttpStatusCodeResult PostSensor([Bind(Include = "NameID,PhoneNumber,Location,Description,CustumerName")]Sensor sensor)
        {
            if (ModelState.IsValid)
            {
                sensor.Status = SensorStatus.Green;
                sensor.InstallationTime = DateTime.Now;
                sensor.IsForGuests = false;
                if (isCustumer)
                {
                    sensor.Custumer = currentCustumer;
                    sensor.CustumerName = currentCustumer.Name;
                    currentCustumer.Sensors.Add(sensor);
                }
                if(isAdministrator)
                {
                    var AssignUser = db.Custumers.FirstOrDefault(x => x.Name == sensor.CustumerName); 
                    if (AssignUser != null)
                    {
                        sensor.Custumer = AssignUser;
                        AssignUser.Sensors.Add(sensor);
                    }
                    else
                    {
                        sensor.IsForGuests = true;
                        currentAdministrator.Sensors.Add(sensor);
                      
                    }
                }
                db.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        // GET: Sensors
        public ActionResult Index()
        {   
            
            return View(db.GetSensersByUserName(User.Identity.Name));
        }

        // GET: Sensors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sensor sensor = db.Sensors.Find(id);
            if (sensor == null)
            {
                return HttpNotFound();
            }
            return View(sensor);
        }

        // GET: Sensors/Create
        public ActionResult Create()
        {
      
            return View();
        }

        // POST: Sensors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SecondID,PhoneNumber,Location,Description")] Sensor sensor)
        {
            if (ModelState.IsValid)
            {
                currentCustumer.Sensors.Add(sensor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

          //  ViewBag.AdminControlID = new SelectList(db.AdminControls, "AdminControlID", "AdminName", sensor.AdminControlID);
            return View(sensor);
        }

        // GET: Sensors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sensor sensor = db.Sensors.Find(id);
            if (sensor == null)
            {
                return HttpNotFound();
            }
         //   ViewBag.AdminControlID = new SelectList(db.Adm, "AdminControlID", "AdminName", sensor.AdminControlID);
            return View(sensor);
        }

        // POST: Sensors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SecondID,PhoneNumber,Location,Description")] Sensor sensor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sensor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        
            return View(sensor);
        }

        // GET: Sensors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sensor sensor = db.Sensors.Find(id);
            if (sensor == null)
            {
                return HttpNotFound();
            }
            return View(sensor);
        }

        // POST: Sensors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sensor sensor = db.Sensors.Find(id);
            db.Sensors.Remove(sensor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
