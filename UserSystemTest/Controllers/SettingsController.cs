﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UserSystemTest.Models;

namespace UserSystemTest.Controllers
{   
    [Authorize(Roles ="Custumer")]
    public class SettingsController : Controller
    {
        // GET: Settings
        private ApplicationDbContext db = new ApplicationDbContext();
        public Custumer currentCustumer { get { return db.Custumers.FirstOrDefault(x => x.Name == User.Identity.Name);  } }
        public void CreateSelectListForNotifications()
        {
            
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> itemsCopy = new List<SelectListItem>() { };
           
            items.Add(new SelectListItem() { Text = "Once a day", Value = "0" } );
            items.Add(new SelectListItem() { Text = "Once in three days", Value = "1" });
            items.Add(new SelectListItem() { Text = "Once a week", Value = "2" });

            items.Add(new SelectListItem() { Text = "Once a month", Value = "3" });
            items.Add(new SelectListItem() { Text = "Once occures error", Value = "4" });
            items.Add(new SelectListItem() { Text = "Once danger level water or batery", Value = "5" });
            items.ForEach(x => {
                if ((int.Parse(x.Value) == (int)currentCustumer.notification))
                {
                    itemsCopy.Insert(0, x);
                }
                else
                {
                    itemsCopy.Add(x);
                }
            } );

            ViewBag.SelectListForNotifications = itemsCopy;
        }
        public void CreateSelectListForAdministrator()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            int count = 0;
            items.Add(new SelectListItem { Text = "Click to find user", Value = count.ToString(), Selected = true });
            foreach (var item in db.Workers)
            {
                count++;
                if (!currentCustumer.Workers.Contains(item))
                {
                    items.Add(new SelectListItem { Text = item.Name, Value = count.ToString() });
                }
            }
            if(count == 0)
            {
                items.Clear();
                items.Add(new SelectListItem { Text = "There are no user to add", Value = count.ToString(), Selected = true });
            }
            ViewBag.SelectListForAdministrator = items;
        
        }
        [HttpPost]
        public HttpStatusCodeResult PostPhoneForNotifications(string Email, int Notification)
        {
            if (!string.IsNullOrWhiteSpace(Email))
            {
                string Email2 = currentCustumer.EmailForNotifications;
                if (Email2 != Email)
                {
                    
                        Util.MailSender.SendMail(Email, string.Format("Your email has just changed from {0} to {1}", Email2, Email));
                        currentCustumer.EmailForNotifications = Email;
                }
                currentCustumer.notification = (Notification)(Notification);
                db.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        public ActionResult Index()
        {
            CreateSelectListForNotifications();
            CreateSelectListForAdministrator();
            var users = currentCustumer.Workers.Select( x=> String.Concat( x.Name ) ).ToList();
            Debug.WriteLine(currentCustumer.EmailForNotifications);
            ViewBag.EmailForNotifications = currentCustumer.EmailForNotifications;
            return View(users);
        }
        [HttpPost]
        public HttpStatusCodeResult PostUser(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var worker = db.Workers.FirstOrDefault(x => x.Name == name);
                currentCustumer.Workers.Add(worker);
                db.SaveChanges();
                // currentCustumer.notofication 
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        [HttpPost]
        public void DeleteUser(string name)
        {
            if (User.IsInRole("Custumer"))
            {
                var worker = db.Workers.FirstOrDefault(x => x.Name == name);
                currentCustumer.Workers.Remove(worker);
                db.SaveChanges();
            }
        }
        public ActionResult GetUsers()
        {
            return Json(currentCustumer.Workers.Select(x => new { name = x.Name }), JsonRequestBehavior.AllowGet);
        }
    }
}