﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UserSystemTest.Models;

namespace UserSystemTest.Controllers
{
    public class DataController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult GetLastDatas()
        {
            var data = db.GetSensersByUserName(User.Identity.Name).SelectMany(x => x.Datas).ToList();
            var LastData = data.Select(x => new { WhenIsAddedTime = x.WhenIsAddedTime.ToShortDateString(), Water = x.Water, NameID = x.Sensor.NameID, Batery = x.Batery }).ToList();
            return Json(LastData, JsonRequestBehavior.AllowGet);
        }
        //get data for plot last statistic 
        public ActionResult GetLastDatasAll(int ID)
        {

            var data = db.Sensors.Where(x=>x.SensorID == ID).Select(x => x.Datas.LastOrDefault()).ToList().Where(x=>x!=null).ToList();
            if (data.Count > 0)
            {
                data.Sort(GetComparer());
                var LastData = data.Select(x => new { WhenIsAddedTime = x.WhenIsAddedTime.ToShortDateString(), Water = x.Water, Batery = x.Batery, NameID = x.Sensor.NameID }).ToList();
                return Json(LastData, JsonRequestBehavior.AllowGet);
            }
            else
                return HttpNotFound();
        }
        public Comparison<Data> GetComparer()
        {
            return new Comparison<Data>((Data x, Data y) => {
                if (x.WhenIsAddedTime < y.WhenIsAddedTime)
                {
                    return -1;
                }
                if (x.WhenIsAddedTime > y.WhenIsAddedTime)
                {
                    return 1;
                }
                return 0;
            });
        }
        public ActionResult GetDataByRange(string startTime, string endTime, int id)
        {
            DateTime startTimeNet = DateTime.Parse(startTime);
            DateTime endTimeNet = DateTime.Parse(endTime);
            var data = db.Sensors.Where(x=>x.SensorID == id).SelectMany(x=>x.Datas).Where(x=>(x.WhenIsAddedTime >= startTimeNet && x.WhenIsAddedTime <= endTimeNet )).ToList();
            if (data.Count > 0)
            {
                data.Sort(GetComparer());
                var dataFormated = data.Select(x => new { WhenIsAddedTime = x.WhenIsAddedTime.ToShortDateString(), Water = x.Water.ToString(), Batery = x.Batery.ToString() }).ToList();
                return Json(dataFormated, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult GetDatasForPlot(int ID)
        {

            var data = db.Sensors.Where(x => x.SensorID == ID).SelectMany(x => x.Datas).Where(x=>x!=null).ToList();
            if (data.Count > 0)
            {
                data.Sort(GetComparer());
                var LastData = data.Select(x => new { WhenIsAddedTime = x.WhenIsAddedTime.ToShortDateString(), Water = x.Water.ToString(), Batery = x.Batery.ToString() }).ToList();
                return Json(LastData, JsonRequestBehavior.AllowGet);
            }
            else
                return HttpNotFound();
        }
        // GET: Data
        public ActionResult Index()
        {
            var data = db.Data.Include(d => d.Sensor);
            return View(data.ToList());
        }

        // GET: Data/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Data data = db.Data.Find(id);
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        // GET: Data/Create
        public ActionResult Create()
        {
            ViewBag.SensorID = new SelectList(db.Sensors, "SensorID", "SensorID");
            return View();
        }

        // POST: Data/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DataID,SensorID,WhenIsAddedTime,Batery,Water")] Data data)
        {
            if (ModelState.IsValid)
            {
                db.Data.Add(data);
                db.SaveChanges();
                Sensor sensor = User.GetSensorByData(data);
       

                if (sensor?.Custumer?.notification == Notification.DangerLevelWaterOrBatery)
                {
                    if (data.Water <= 10)
                    {
                      
                            Util.MailSender.SendMail(sensor.Custumer.EmailForNotifications, string.Format("The sensor ID = {0} has low level water {1}%", sensor.NameID, data.Water));
                       
                    }
                    if (data.Batery <= 10)
                    {
                    
                            Util.MailSender.SendMail(sensor.Custumer.EmailForNotifications, string.Format("The sensor ID = {0} has low level batery {1}%", sensor.NameID, data.Batery));
         
                    }
                }
                return RedirectToAction("Index");
            }

            ViewBag.SensorID = new SelectList(db.Sensors, "SensorID", "NameID", data.SensorID);
            return View(data);
        }

        // GET: Data/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Data data = db.Data.Find(id);
            if (data == null)
            {
                return HttpNotFound();
            }
            ViewBag.SensorID = new SelectList(db.Sensors, "SensorID", "NameID", data.SensorID);
            return View(data);
        }

        // POST: Data/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DataID,SensorID,WhenIsAddedTime,Batery,Water")] Data data)
        {
            if (ModelState.IsValid)
            {
                db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SensorID = new SelectList(db.Sensors, "SensorID", "NameID", data.SensorID);
            return View(data);
        }

        // GET: Data/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Data data = db.Data.Find(id);
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        // POST: Data/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Data data = db.Data.Find(id);
            db.Data.Remove(data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}   
