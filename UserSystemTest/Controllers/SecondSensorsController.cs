﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UserSystemTest.Models;

namespace UserSystemTest.Controllers
{
    public class SecondSensorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SecondSensors
        public ActionResult Index()
        {
            var sensors = db.Sensors.Include(s => s.Custumer);
            return View(sensors.ToList());
        }

        // GET: SecondSensors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sensor sensor = db.Sensors.Find(id);
            if (sensor == null)
            {
                return HttpNotFound();
            }
            return View(sensor);
        }

        // GET: SecondSensors/Create
        public ActionResult Create()
        {
            ViewBag.CustumerID = new SelectList(db.Custumers, "CustumerID", "Name");
            return View();
        }

        // POST: SecondSensors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SensorID,CustumerID,AdministratorID,NameID,IsForGuests,Status,InstallationTime,PhoneNumber,Location,Description,CustumerName")] Sensor sensor)
        {
            if (ModelState.IsValid)
            {
                db.Sensors.Add(sensor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustumerID = new SelectList(db.Custumers, "CustumerID", "Name", sensor.CustumerID);
            return View(sensor);
        }

        // GET: SecondSensors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sensor sensor = db.Sensors.Find(id);
            if (sensor == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustumerID = new SelectList(db.Custumers, "CustumerID", "Name", sensor.CustumerID);
            return View(sensor);
        }

        // POST: SecondSensors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SensorID,CustumerID,AdministratorID,NameID,IsForGuests,Status,InstallationTime,PhoneNumber,Location,Description,CustumerName")] Sensor sensor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sensor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustumerID = new SelectList(db.Custumers, "CustumerID", "Name", sensor.CustumerID);
            return View(sensor);
        }

        // GET: SecondSensors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sensor sensor = db.Sensors.Find(id);
            if (sensor == null)
            {
                return HttpNotFound();
            }
            return View(sensor);
        }

        // POST: SecondSensors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sensor sensor = db.Sensors.Find(id);
            db.Sensors.Remove(sensor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
