﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserSystemTest.Models;

namespace UserSystemTest.Controllers
{
    public class TrackController : Controller
    {
        // GET: Track
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.Value = db.GetUserByName(User.Identity.Name).showGuestSensors == true ? "checked='Checked'" : "";
                //Debug.WriteLine(ViewBag.Value.ToString());
                CreateSelectListForAdministrator();
            }
            return View();
        }
        public void CreateSelectListForAdministrator()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            int count = 0;
            items.Add(new SelectListItem { Text = "For Guests", Value = count.ToString(), Selected = true });
            foreach (var item in db.Custumers)
            {
                count++;
                items.Add(new SelectListItem { Text = item.Name, Value = count.ToString() });
            }
            ViewBag.SelectListForAdministrator = items;

            
        }
    }
}