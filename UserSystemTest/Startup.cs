﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Mvc;
using UserSystemTest.Models;

[assembly: OwinStartupAttribute(typeof(UserSystemTest.Startup))]
namespace UserSystemTest
{
    
    public partial class Startup
    {
        
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
           
        }

    }
}
