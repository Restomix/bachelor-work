﻿function CalculateAverageData(data) {
    var summWater = data.reduce(function (acc, obj) { return +acc + +obj.Water; }, 0);
    var summBatery = data.reduce(function (acc, obj) { return +acc + +obj.Batery; }, 0);
    var averageWater = Math.round(summBatery / data.length);
    var averageBatery = Math.round(summWater / data.length);
    var lastWater = data.slice(-1).pop().Water;
    var lastBatery = data.slice(-1).pop().Batery;
    var table = $("#AverageAndLastDataTable tbody");
    $("#AverageAndLastDataTable tbody > tr").remove();
    table.append(
        "<tr><td><b>Last data</b></td><td>" + lastWater + "</td><td>" + lastBatery + "</td></tr><tr><td><b>Average data</b></td><td>" + averageWater + "</td><td>"+averageBatery+"</td></tr>");
}
$(function () {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        var url = document.location.origin + "/Data/GetDataByRange/";
        var parameters = { startTime: start.format('MMMM D, YYYY').toString(), endTime: end.format('MMMM D, YYYY').toString(), id: window.selectedID };

        var answ = $.getJSON(url, parameters, function (data) {
            CalculateAverageData(data);
            var ArrayDates = new Array();
            var ArrayWater = new Array();
            var ArrayBatery = new Array();
            Plotly.purge("DivPlotId");
            for (var i = 0; i < data.length; i++) {
                ArrayDates.push(data[i].WhenIsAddedTime);
                ArrayBatery.push(data[i].Batery);
                ArrayWater.push(data[i].Water);
            }
            var Batery = {
                x: ArrayDates,
                y: ArrayBatery,
                type: 'scatter',
                name: "Batery"
            };

            var Water = {
                x: ArrayDates,
                y: ArrayWater,
                type: 'scatter',
                name: 'Water'
            };

            var data = [Water, Batery];
            $("#DivForPlot").show();
            Plotly.plot('DivPlotId', data);
            //CalculateAverageData(data);


        });
        answ.fail(() => { if (selectedID != null) { BuildPlot(); alert("You've selected wrong range of date, the plot was created by default"); } });   
        //
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});