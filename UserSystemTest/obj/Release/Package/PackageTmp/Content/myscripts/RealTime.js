﻿$(function () {
    $("#real-time").on("change", function () {
        if ($(this).is(":checked")) {
            interval = setInterval(() => {
                BuildPlot();
            }, 1000);
        }
        else {
            if (interval != null) {
                clearInterval(interval);
            }
        }
    });

});
$(function () {
    $("#guest-sensors").on("change", function () {
        var url = document.location.origin + "/Sensors/ChangeStateForGuestSensors";
        if ($(this).is(":checked")) {

            $.post(url, { state: true }).done(UpdateSensorsTable());
        }
        else {

            $.post(url, { state: false }).done(UpdateSensorsTable());
        }
        
    });
});