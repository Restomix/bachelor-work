﻿$('#submitButton').click(function () {
    var errors = 0;
    $("#menu2 :input").not("button").map(function () {
        if (!$(this).val()) {
            $(this).addClass('warning');
            errors++;
        } else if ($(this).val()) {

            $(this).removeClass('warning');
        }
    }); 
    if (errors > 0) {
        $('#errorwarn').text("All fields are required");
    }
    else if (errors == 0) {
        PostSensor();
        $('#errorwarn').removeClass('warning').addClass('success').text("Success");
        $(':input', '#menu2').val('');
    }
});
function PostSensor() {

    var description = $('input[name=Description]').val();
    var phoneNumber = $("input[name='PhoneNumber']").val();
    var location = $("input[name='Location']").val();
    var id = $("input[name='NameID']").val();
    var custumerName = $("#SelectListForAdministrator option:selected").text();
    var Data = { Description: description, PhoneNumber: phoneNumber, Location: location, NameID: id, CustumerName: custumerName };
    var url = document.location.origin + "/Sensors/PostSensor";
    $.post(url, Data);

}
mymap.on('click', onMapClick);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

function onMapClick(e) {

    if (marker != null) {
        mymap.removeLayer(marker);
    }
    marker = L.marker(e.latlng, { draggable: true }).addTo(mymap);
    marker.bindPopup("<b>Give me description</b>").openPopup();

    $("#inputForLocationFake").val(e.latlng.lat.toString().substr(0, 8) + " x " + e.latlng.lng.toString().substr(0, 8));
  
}