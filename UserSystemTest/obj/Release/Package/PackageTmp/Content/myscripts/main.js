﻿        class Current {
        this.mymap = L.map('mapid').setView([51.505, -0.09], 13);
        this.marker = null;
        this.markers = new Array();
        function onMapClick(e) {

            if (marker != null) {
                mymap.removeLayer(marker);
            }
            marker = L.marker(e.latlng, { draggable: true }).addTo(mymap);
            marker.bindPopup("<b>Give me description</b>").openPopup();

            $("#inputForLocationFake").val(e.latlng.lng.toString().substr(0, 8) + " x " + e.latlng.lat.toString().substr(0, 8));
            $("#inputForLocation").val(e.latlng.lng.toString() + "x" + e.latlng.lat.toString());

        }
        Constructor() {
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="http://mapbox.com">Mapbox</a>',
                id: 'mapbox.streets'
             }).addTo(mymap);
            mymap.on('click', onMapClick);
        }
        var s = new Cureent();

$("a[data-toggle='tab']").on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
        //selector for SENSORS
        if (target == "#menu1") {
                var url = document.location.origin + "/Sensors/GetSensors";
                $.getJSON(url, function (data) {
                $("#SensorsTable tbody > tr").remove();
                for (var i = 0; i < data.length; i++) {
                    alert(data[i].SensorID.toString());
                    $("<tr class='sensor' ><td>" + data[i].SensorID + "</td><td>" + data[i].PhoneNumber + "</td><td class='location'>" + data[i].Location + "</td><td>" + data[i].Description + "</td><td><a href='" + document.location.origin + "/Sensors/Edit/" + data[i].SensorID+"'>Edit</a></td></tr>").appendTo($("#SensorsTable tbody"));

                      var long = data[i].Location.split("x")[0];
                      var lat = data[i].Location.split("x")[1];
  
                      var mark = new L.marker([lat, long]);
                      mymap.setView([lat, long]);
                      markers.push(mark);
                      mark.addTo(mymap);
                      mark.bindPopup(data[i].Description);
                }
                
                $('.sensor').click(function () {
                    var loc = $(this).find(".location").text();
                    var long = loc.split('x')[0];
                    var lat = loc.split('x')[1];
                    mymap.closePopup();
                    for (let i = 0; i < markers.length; i++) {

                        if (markers[i]._latlng.lat == lat && markers[i]._latlng.lng == long) {
                            mymap.setView([lat, long]);
                            markers[i].openPopup();
                            break;
                        }

                    }

                    var selectedID = $(this).children("td").first().text();
                    var url = document.location.origin+"/Data/GetDatasForPlot/" + selectedID;
                    $.getJSON(url, function (data) {

                        var ArrayDates = new Array();
                        var ArrayWater = new Array();
                        var ArrayBatery = new Array();
                        for (var i = 0; i < data.length; i++) {
                            ArrayDates.push(data[i].WhenIsAddedTime);
                            ArrayBatery.push(data[i].Batery);
                            ArrayWater.push(data[i].Water)
                        }
                        var Batery = {
                            x: ArrayDates,
                            y: ArrayWater,
                            type: 'scatter',
                            name: "Batery"
                        };

                        var Water = {
                            x: ArrayDates,
                            y: ArrayWater,
                            type: 'scatter',
                            name: 'Water'
                        };

                        var data = [Water, Batery];
                        Plotly.newPlot('myDiv', data);

                    });
                    /*get all statistic*/
                    var url = document.location.origin + "/Data/GetLastDatasAll/" + selectedID;
                    $.getJSON(url, function (data) {

                        $("#LastDatasTableAll tbody > tr").remove();
                        $("#LastDatasTableAll").show();
                        for (var i = 0; i < data.length; i++) {
                            $("<tr class='LastData' ><td>" + data[i].SensorID + "</td><td>" + data[i].WhenIsAddedTime + "</td><td class='Location'>" + data[i].Batery + "</td><td>" + data[i].Water + "</td></tr>").appendTo($("#LastDatasTableAll tbody"));

                        }

                    });

                });
              
            });
            
    }
        if (target == "#home")
        {   
            var url = document.location.origin + "/Data/GetLastDatas";
            $.getJSON(url, function (data) {
                $("#LastDatasTable tbody > tr").remove();
                for (var i = 0; i < data.length; i++) {
                    $("<tr class='LastData' ><td>" + data[i].SensorID + "</td><td>" + data[i].WhenIsAddedTime + "</td><td class='Location'>" + data[i].Batery + "</td><td>" + data[i].Water + "</td></tr>").appendTo($("#LastDatasTable tbody"));
                
                }

            });

        }
});



